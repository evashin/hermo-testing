(function(angular) {
	 'use strict';
var App = angular.module("myApp", ["ngResource","ngRoute"]);



App.controller("homecontrol", function ($scope, homeService, bannerService, $route, $routeParams, $location) {
      
	$scope.product = homeService.query(function (data) {

		console.log("Got all product", data);

	});

	$scope.banner = bannerService.query(function (data) {

		console.log("Got all product", data);

	});

     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
     
});


App.controller("productctrl", function ($scope, $resource, $sce,$routeParams) {
    var productResource = $resource('https://api.hermo.my/test/product/:id', {
        id: '@id'
    });
    productResource.get({
        id: 19977
    }, function (data) {
        $scope.detail = data;

        $scope.trustedHtml1 = $sce.trustAsHtml($scope.detail.description);
        $scope.trustedHtml2 = $sce.trustAsHtml($scope.detail.usage);
        $scope.trustedHtml3 = $sce.trustAsHtml($scope.detail.photos);


    });
    productResource.get({
        id: 1556
    }, function (data) {
        $scope.detail1 = data;

    });
    productResource.get({
        id: 2122
    }, function (data) {
        $scope.detail2 = data;

    });
    productResource.get({
        id: 10505
    }, function (data) {
        $scope.detail3 = data;

    });
    productResource.get({
        id: 10532
    }, function (data) {
        $scope.detail4 = data;

    });




});





App.config(
  function($routeProvider) {

    $routeProvider
        .when('/productdetail', {
       	    templateUrl: 'productdetails/details.html',
	        controller: 'productctrl'
        })
        .when('/', {
            templateUrl: 'home.html', 
            controller: 'homecontrol'
        })
});




App.factory("homeService", function ($resource) {

	return $resource('https://api.hermo.my/test/best-selling', {
		method: 'GET'
	});

});


App.factory("bannerService", function ($resource) {

	return $resource('https://api.hermo.my/test/banner', {
		method: 'GET'
	});

});


})(window.angular);